import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientHomeComponent } from './pages/patient-home/patient-home.component';
import { PatientRoutingModule } from './patient-routing.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [PatientHomeComponent],
  imports: [CommonModule, PatientRoutingModule, SharedModule],
  exports: [RouterModule]
})
export class PatientModule {}
