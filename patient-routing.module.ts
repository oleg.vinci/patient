import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PatientHomeComponent } from './pages/patient-home/patient-home.component';

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: PatientHomeComponent
  }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule {}
